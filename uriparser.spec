Summary:        URI parsing library - RFC 3986
Name:           uriparser
Version:        0.9.7
Release:        5%{?dist}
License:        BSD
URL:            https://uriparser.github.io/
Source0:        https://github.com/%{name}/%{name}/releases/download/%{name}-%{version}/%{name}-%{version}.tar.bz2

Patch0001:      CVE-2024-34402-Protect-against-integer-overflow-in-ComposeQueryEngi.patch
Patch0002:      CVE-2024-34403-Protect-against-integer-overflow-in-ComposeQueryMall.patch

BuildRequires:  make cmake gcc-c++
BuildRequires:  doxygen graphviz gtest-devel


%description
This package provides a strictly RFC 3986 compliant URI parsing library written in C. uriparser 
is cross-platform, fast, supports Unicode and is licensed under the New BSD license.


%package devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description devel
This package provides libraries and header files for developing applications that use %{name}.


%prep
%autosetup -p1
# remove qhelpgenerator requires
sed -i 's/GENERATE_QHP\ =\ yes/GENERATE_QHP\ =\ no/g' doc/Doxyfile.in


%build
%cmake
%cmake_build


%install
%cmake_install


%check
%ctest


%files
%license COPYING
%doc THANKS AUTHORS ChangeLog
%{_bindir}/uriparse
%{_libdir}/lib%{name}.so.1*
%{_libdir}/cmake/%{name}-%{version}/

%files devel
%license COPYING
%doc %{_docdir}/%{name}/html
%{_includedir}/%{name}/
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/lib%{name}.pc


%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-5
- Rebuilt for loongarch release

* Mon May 6 2024 Shuo Wang <abushwang@tencent.com> - 0.9.7-4
- backport patch to fix CVE-2024-34402 CVE-2024-34403
- Protect against integer overflow in ComposeQueryEngine and ComposeQueryMallocExMm

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-2
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Apr 21 2023 Wang Guodong <gordonwwang@tencent.com> - 0.9.7-1
- initial build
